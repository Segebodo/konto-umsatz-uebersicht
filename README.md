# Konto Umsatz Übersicht

Dieses Projekt hat das Ziel, die Umsätze auf einem Bankkonto übersichtlich darzustellen und so die Frage: "Wofür gebe ich mein Geld aus?" schnell zu beantworten.
Das Skript ist für den Sparkassen-Csv-Export geschrieben und sollte relativ einfach auf andere Csv-Exporte anpassbar sein.

Um eine Übersicht zu bekommen werden Ausgaben nach Akteuren sortiert. Mehrere Akteure können unter einer Kategorie zusammengefasst werden (siehe `actor_topics`).
Das Skript sortiert die Kategorien nach größtem Umsatz.

Die monatliche Ansicht zeigt alle Ausgaben, die größten Kategorien explizit, die kleineren unter "Andere".
Für die weitere Recherche können einfache Queries geschrieben werden (ein Beispiel ist vorhanden), mit denen z.B. ein Monat oder eine Kategorie aufgeschlüsselt werden kann.
