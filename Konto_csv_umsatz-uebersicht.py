import sys, csv, locale, tabulate
from datetime import datetime
from collections import Counter
import numpy as np, matplotlib.pyplot as plt

# input parameters
bank_csv_file = 'umsatz.CSV'
value_column = 'Betrag'
locale.setlocale(locale.LC_ALL, 'de_DE')
actor_column= 'Beguenstigter/Zahlungspflichtiger'
date_column= 'Buchungstag'
date_format= '%d.%m.%y'
message_column = "Verwendungszweck"

# display parameters
month_format = '%y %b'
topic_column = "Kategorie"
clean_columns = [date_column, value_column, "Waehrung", topic_column, actor_column, message_column]
actor_topics = {"DB Vertrieb GmbH": "Öffis",
                "DB": "Öffis"
                }
important_actor_count = 18 # amount of actors shown in monthly table
table_head_len = 6 # truncate table headers
                      

# debugging
debug_logging = False
def debug(*string):
    if debug_logging:
        print(' Debug:', *string);

def Topic_or_actor(actor):
    if actor in actor_topics:
        return actor_topics[actor]
    else:
        return "*" + actor
def Topic(actor):
    if actor in actor_topics:
        return actor_topics[actor]
    else:
        return ''
def Table_row(transaction):
    return [transaction[column] for column in clean_columns]
def print_transactions(transactions, header=clean_columns):
    rows = [[transaction[key] for key in header] for transaction in transactions]
    print()
    print(tabulate.tabulate(rows, header)) 

print('\nrunning ' + sys.argv[0]+ ' on ' + bank_csv_file + '\n');

## read file
transactions = []
actors = {}
header = []

with open(bank_csv_file) as input_file:
    csv_reader = csv.reader(input_file, delimiter=';', quotechar='"')
    try:
        header = next(csv_reader)
        debug(header)
        debug_counter = 1
        for row in csv_reader:
            transaction = dict(zip(header, row))
            # type conversions, clean up
            transaction[value_column] = float(locale.atof(transaction[value_column]))
            transaction['datetime'] = datetime.strptime(transaction[date_column], date_format)
            transaction['month'] = transaction["datetime"].strftime(month_format)
            transaction[message_column] = transaction[message_column][:50]
            transaction[topic_column] = Topic(transaction[actor_column])
            
            if debug_logging and (debug_counter > 0):
                print(row)
                print_transactions([transaction])
                debug_counter -= 1
            
            transactions.append(transaction)
            
            actor = transaction[actor_column]
            if actor in actors:
                actors[actor][1].append(transaction)
            else:
                actors[actor] = [0, [transaction]]
    except csv.Error as e:
        sys.exit('Could not read bank csv file - Error at {}, line {}: {}'.format(filename, reader.line_num, e))


## important topics/actors
topic_abs_sum = {}
topic_sum = {}
for actor in actors.keys():
    topic = Topic_or_actor(actor)
    actor_abs_sum = sum([abs(transaction[value_column]) for transaction in actors[actor][1]])
    actor_sum = sum([transaction[value_column] for transaction in actors[actor][1]])
    if not(topic in topic_abs_sum.keys()):
        topic_abs_sum[topic] = actor_abs_sum
        topic_sum[topic] = actor_sum
    else:
        topic_abs_sum[topic] += actor_abs_sum
        topic_sum[topic] += actor_sum
topicCounter = Counter(topic_abs_sum)
important_topics = [actor for actor, value in topicCounter.most_common(important_actor_count)] 
important_incoming_topics = list(filter(lambda actor: topic_sum[actor] > 0, important_topics))
important_outgoing_topics = list(filter(lambda actor: topic_sum[actor] < 0, important_topics))
print()
table_rows = [[actor, topic_sum[actor]] for actor, abs_sum in topicCounter.most_common(70)]
print(tabulate.tabulate(table_rows, ["Kategorie/" + actor_column, value_column]))

## monthly report
monthly_header = ['month', *[actor[:table_head_len] for actor in important_incoming_topics], 'other (+)', *[actor[:table_head_len] for actor in important_outgoing_topics], 'other (-)']
months = []
monthly_values = {}
for transaction in transactions:
    month = transaction['month']
    topic = Topic_or_actor(transaction[actor_column])
    value = transaction[value_column]
    if not (month in months):
        months.append(month)
        monthly_values[month] = np.zeros(len(important_topics)+2)
    if topic in important_incoming_topics:
        i = important_incoming_topics.index(topic)
        monthly_values[month][i] += value
    elif topic in important_outgoing_topics:
        i = important_outgoing_topics.index(topic) + len(important_incoming_topics) +1
        monthly_values[month][i] += value
    else:
        if value > 0:
            monthly_values[month][len(important_incoming_topics)] += value
        if value < 0:
            monthly_values[month][-1] += value
#Average
monthly_average = np.zeros(len(important_topics)+2)
for month in months:
    monthly_average += monthly_values[month]
monthly_average = monthly_average / len(months)
months.append("average")
monthly_values["average"] = monthly_average

#round
for month in months:
    monthly_values[month] = list(np.round_(monthly_values[month]))
    
monthly_rows = [[month, *monthly_values[month]] for month in months]
debug(monthly_rows)
print()
print(tabulate.tabulate(monthly_rows, monthly_header))

## example query
print()
print("Custom Query:")
print()
table_rows = [Table_row(transaction) for transaction in filter(lambda transaction: transaction["month"] == "22 Juli", transactions)]
print(tabulate.tabulate(table_rows, clean_columns))
